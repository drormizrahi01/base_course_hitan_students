package iaf.ofek.hadracha.base_course.web_server.Data;

public class Coordinates {
    public double lat;
    public double lon;

    @Override
    public String toString() {
        return String.format("N%.6fE%.6f",lat, lon);
    }

    public Coordinates(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
    }

    public Coordinates() {
    }

    //setters and getters that Gavri added after changing lat and lon to private

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public void addTo_Lat(double addition) {
        this.lat += addition;
    }

    public void addTo_Lon(double addition) {
        this.lon += addition;
    }

}

