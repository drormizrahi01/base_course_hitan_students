package iaf.ofek.hadracha.base_course.web_server.AirSituation;

import iaf.ofek.hadracha.base_course.web_server.Utilities.GeographicCalculations;
import iaf.ofek.hadracha.base_course.web_server.Utilities.RandomGenerators;
import iaf.ofek.hadracha.base_course.web_server.Data.Coordinates;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * A simulator that manages airplanes and randomly advances them in position over time
 */
@Service
public class SimulativeAirSituationProvider implements AirSituationProvider {

    private static final double CHANCE_FOR_NUMBER_CHANGE = 0.005;
    private static final double CHANCE_FOR_AZIMUTH_CHANGE = 0.05;
    private static int STEP_SIZE = 15;
    private static int SIMULATION_INTERVAL_MILLIS = 100;
    private double LAT_MIN = 29.000;
    private double LAT_MAX = 36.000;
    private double LON_MIN = 32.000;
    private double LON_MAX = 46.500;
    private static final double AZIMUTH_STEP = STEP_SIZE / (2000.0 / SIMULATION_INTERVAL_MILLIS);

    // Scheduler to run advancement task repeatedly
    private ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    // Object that acts as a lock for the updates
    private final Object LOCK = new Object();
    private final Random RANDOM = new Random();
    private final RandomGenerators RANDOM_GENERATORS;
    private final GeographicCalculations GEOGRAPHIC_CALCULATIONS;

    // The current air situation
    private List<Airplane> airplanes = new ArrayList<>();
    private int lastId = 0;

    private static final int CALCULATE_SECONDARY_AZIMUTH = 180;
    private static final int MAX_DESTINATION = 500;
    private static final int PLAINS_AMOUNT = 80;
    private static final int ROTATION = 360;
    private static final int SPEED_MODIFIER = 100000;


    public SimulativeAirSituationProvider(RandomGenerators randomGenerators, GeographicCalculations geographicCalculations) {
        this.RANDOM_GENERATORS = randomGenerators;
        this.GEOGRAPHIC_CALCULATIONS = geographicCalculations;

        for (int i = 0; i < PLAINS_AMOUNT; i++) {
            createNewAirplaneInstanceInRandomPosition();
        }

        executor.scheduleAtFixedRate(this::UpdateSituation, 0, SIMULATION_INTERVAL_MILLIS, TimeUnit.MILLISECONDS);
    }

    // all airplane kinds that can be used
    private List<AirplaneKind> airplaneKinds = AirplaneKind.LeafKinds();

    private void createNewAirplaneInstanceInRandomPosition() {
        AirplaneKind kind = airplaneKinds.get(RANDOM.nextInt(airplaneKinds.size()));

        Airplane airplane = new Airplane(kind, lastId++);
        airplane.setCoordinates(new Coordinates(RANDOM_GENERATORS.generateRandomDoubleInRange(LAT_MIN, LAT_MAX),
                RANDOM_GENERATORS.generateRandomDoubleInRangeWithNormalDistribution(LON_MIN, LON_MAX)));
        airplane.setAzimuth(RANDOM_GENERATORS.generateRandomDoubleInRange(0,ROTATION));
        airplane.setVelocity(RANDOM_GENERATORS.generateRandomDoubleInRange(40, 70)*airplane.getAirplaneKind().getVelocityFactor());

        airplanes.add(airplane);
    }

    private void UpdateSituation() {
        try {
            synchronized (LOCK) {
                if (RANDOM.nextDouble() < CHANCE_FOR_NUMBER_CHANGE) { // chance to remove an airplane
                    int indexToRemove;
                    do {
                        indexToRemove = RANDOM.nextInt(airplanes.size());
                    } while (airplanes.get(indexToRemove).isAllocated());// don't remove allocated airplane
                    airplanes.remove(indexToRemove);
                }

                airplanes.forEach(airplane -> {
                    airplane.radialAcceleration = calculateNewRadialAcceleration(airplane);

                    airplane.setAzimuth(airplane.getAzimuth() + airplane.radialAcceleration);
                    airplane.getCoordinates().addTo_Lat(Math.sin(worldAzimuthToEuclidRadians(airplane.getAzimuth())) * airplane.getVelocity() / SPEED_MODIFIER);
                    airplane.getCoordinates().addTo_Lon(Math.cos(worldAzimuthToEuclidRadians(airplane.getAzimuth())) * airplane.getVelocity() / SPEED_MODIFIER);
                    if (airplane.getCoordinates().getLat() < LAT_MIN || airplane.getCoordinates().getLat() > LAT_MAX ||
                            airplane.getCoordinates().getLon() < LON_MIN || airplane.getCoordinates().getLon() > LON_MAX)
                        airplane.setAzimuth(airplane.getAzimuth() + 180);

                });

                if (RANDOM.nextDouble() < CHANCE_FOR_NUMBER_CHANGE) { // chance to add an airplane
                    createNewAirplaneInstanceInRandomPosition();
                }
            }
        }
        catch (Exception e){
            System.err.println("Error while updating air situation picture" + e.getMessage());
            e.printStackTrace();
        }
    }

    private double calculateNewRadialAcceleration(Airplane airplane) {

        if (airplane.isAllocated()) {
            Coordinates currLocation = airplane.getCoordinates();
            Coordinates headingTo = airplane.getHeadingTo();


            if (arrivedToDestination(currLocation, headingTo)) {
                airplane.raiseArrivedAtDestinationEvent();
            }

            double azimuthToDestenation = GEOGRAPHIC_CALCULATIONS.azimuthBetween(currLocation, headingTo);
            double differnceOfAzimuth = CALCULATE_SECONDARY_AZIMUTH- GEOGRAPHIC_CALCULATIONS.normalizeAzimuth(azimuthToDestenation - airplane.getAzimuth());

            return (differnceOfAzimuth > 0 ? Math.min(AZIMUTH_STEP*10, differnceOfAzimuth/5) : Math.max(-AZIMUTH_STEP*10, differnceOfAzimuth/5))/2;

        }
        else {
            if (RANDOM.nextDouble() < CHANCE_FOR_AZIMUTH_CHANGE)       // chance for any change
                if (RANDOM.nextDouble() < CHANCE_FOR_AZIMUTH_CHANGE)   // chance for big change
                    return RANDOM_GENERATORS.generateRandomDoubleInRange(-AZIMUTH_STEP, AZIMUTH_STEP);
                else   // small gradual change, with a 66% chance that the size of the acceleration will be reduced
                    return RANDOM_GENERATORS.generateRandomDoubleInRange(0, 1.5 * airplane.radialAcceleration);
            return airplane.radialAcceleration;
        }
    }

    private boolean arrivedToDestination(Coordinates currLocation, Coordinates headingTo) {
        return GEOGRAPHIC_CALCULATIONS.distanceBetween(currLocation, headingTo) < MAX_DESTINATION;
    }

    /**
     * Gets world azimuth - degrees in which 0 is up and increases clockwise and converts it to
     * radians in which 0 is right and increases counter clockwise.
     */
    private double worldAzimuthToEuclidRadians(double azimuth) {
        double inEuclidDegrees = -azimuth + 90;
        return inEuclidDegrees * Math.PI / CALCULATE_SECONDARY_AZIMUTH;
    }

    @Override
    public List<Airplane> getAllAirplanes() {
        synchronized (LOCK){
            return new ArrayList<>(airplanes);
        }
    }
}
