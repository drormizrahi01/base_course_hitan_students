package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.AirSituation.Airplane;

import java.util.List;

public interface EjectionsProvider {
    List<EjectedPilotInfo> getAllEjectedPilotInfo();
}
