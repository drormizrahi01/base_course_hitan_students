package iaf.ofek.hadracha.base_course.web_server.EjectedPilotRescue;

import iaf.ofek.hadracha.base_course.web_server.AirSituation.AirSituationProvider;
import iaf.ofek.hadracha.base_course.web_server.AirSituation.Airplane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ejectedPilotRescue")
public class EjectionsController {

    private EjectionsImporter ejectionsImporter;

    public EjectionsController(@Autowired EjectionsImporter ejectionsImporter) {
        this.ejectionsImporter = ejectionsImporter;
    }

    @GetMapping("/infos")
    public List<EjectedPilotInfo> getEjectionsSituation() {
        return ejectionsImporter.getAllEjectedPilotInfo();
    }

    @GetMapping("/takeResponsibility")
    public void defineRescuer(@RequestParam int ejectionId, @CookieValue("client-id") String clientId) {
        this.ejectionsImporter.findRescuer(ejectionId, clientId);
    }
}
